import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../App';
import { addUser, deleteUser, editUser, getUsers } from './store/users.actions';

export function UsersPage() {
  const dispatch = useDispatch<AppDispatch>();
  const users = useSelector((state: RootState) => state.users.list)
  const error = useSelector((state: RootState) => state.users.error)
  const pending = useSelector((state: RootState) => state.users.isPending)

  useEffect(() => {
    dispatch(getUsers())
  }, [])

  function addUserHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addUser({ name: 'pippo' }))
    }
  }

  return <div>

    { pending && <div>loader...</div>}

    { error && <div className="alert alert-danger">ahia!</div>}

    <input type="text" onKeyDown={addUserHandler} placeholder="add user name"/>
    {
      users.map(u => {
        return <li
          key={u.id}
        >
          {u.name}
          <i className="fa fa-trash"
             onClick={() => dispatch(deleteUser(u.id))}></i>
          <button
            onClick={() => dispatch(editUser({ id: u.id, age: 30}))}
          >change name to XYZ</button>
        </li>
      })
    }
  </div>
}
