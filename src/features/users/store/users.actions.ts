// async

import axios from 'axios';
import { useDispatch } from 'react-redux';
import { AppThunk } from '../../../App';
import { User } from '../../../model/user';
import {
  addUserSuccess,
  deleteUserSuccess,
  editUserSuccess,
  getUsersFailed,
  getUsersSuccess,
  getUserStart
} from './users.store';

export const getUsers =  (): AppThunk => async (dispatch) => {
  dispatch(getUserStart())
  try {
    setTimeout(async () => {
      const res = await axios.get<User[]>('http://localhost:3001/users')
      dispatch(getUsersSuccess(res.data))
    }, 1000)

  } catch(e) {
    dispatch(getUsersFailed())
  }
}

export const addUser = (userToAdd: Partial<User>): AppThunk => async (dispatch) => {
  dispatch({ type: 'users/add'})
  try {
    const res = await axios.post<User>('http://localhost:3001/users', userToAdd)
    dispatch(addUserSuccess(res.data))
  } catch(e) {
    dispatch({ type: 'users/get failed' })
  }
}

export const deleteUser = (idToRemove: number): AppThunk => async (dispatch) => {
  dispatch({ type: 'users/delete', payload: idToRemove})
  try {
    await axios.delete('http://localhost:3001/users/' + idToRemove)
    dispatch(deleteUserSuccess(idToRemove))
  } catch(e) {
    dispatch({ type: 'users/get failed' })
  }
}

export const editUser = (user: Partial<User>): AppThunk => async (dispatch) => {
  dispatch({ type: 'users/edit', payload: user})
  try {
    const res = await axios.patch<User>('http://localhost:3001/users/' + user.id, user)
    dispatch(editUserSuccess(res.data))
  } catch(e) {
    dispatch({ type: 'users/edit failed' })
  }
}

export const getUsers2 = function (): AppThunk {
  return function (dispatch, getState) {
    axios.get('http://localhost:3001/users')
      .then(res => {
        dispatch({ type: 'users/get success', payload: res.data})
      })
      .catch(() => {
        dispatch({ type: 'users/get failed' })
      })
    // chiamata la server
    // dispatch SUCCESS o FAILED
  }
}
