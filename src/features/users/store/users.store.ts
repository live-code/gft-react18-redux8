import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { User } from '../../../model/user';

interface UsersState {
  list: User[],
  error: boolean;
  isPending: boolean;
}

const initialState: UsersState = {
  list: [],
  error: false,
  isPending: false
}

export const userStore = createSlice({
  name: 'users',
  initialState,
  reducers: {
    getUserStart(state) {
      state.isPending = true;
    },
    getUsersSuccess(state, action: PayloadAction<User[]>) {
      state.isPending = false;
      state.error = false;
      state.list = action.payload;
 /*     return {
        ...state,
        list: action.payload
      }*/
    },
    getUsersFailed(state) {
      state.isPending = false;
      state.error = true;
    },
    addUserSuccess(state, action: PayloadAction<User>) {
      state.list.push(action.payload)
    },
    deleteUserSuccess(state, action: PayloadAction<number>) {
      const index = state.list.findIndex(p => p.id === action.payload)
      state.list.splice(index, 1)
    },
    editUserSuccess(state, action: PayloadAction<User>) {
      const index = state.list.findIndex(u => u.id === action.payload.id);
      state.list[index] = {...action.payload}
    }
  }
})

export const {
  getUsersSuccess, getUserStart, getUsersFailed, addUserSuccess, deleteUserSuccess, editUserSuccess
} = userStore.actions
