import { createAction } from '@reduxjs/toolkit';
import axios from 'axios';
import { AppThunk } from '../../../App';
import { Product } from '../../../model/product';

export const getProductsSuccess = createAction<Product[]>('catalog/getProductsSuccess')
export const addProductSuccess = createAction<Product>('catalog/addProductSuccess')
export const deleteProductSuccess = createAction<number>('catalog/deleteProductSuccess')


export const getProducts =  (): AppThunk => async (dispatch) => {
  dispatch({ type: 'products/get'})
  try {
    const res = await axios.get<Product[]>('http://localhost:3001/products')
    dispatch(getProductsSuccess(res.data))
  } catch(e) {
    // dispatch({ type: 'users/get failed' })
  }
}


export const addProduct = (productToAdd: Partial<Product>): AppThunk => async (dispatch) => {
  dispatch({ type: 'product/add', payload: productToAdd})
  try {
    const res = await axios.post<Product>('http://localhost:3001/products', productToAdd)
    dispatch(addProductSuccess(res.data))
  } catch(e) {
    dispatch({ type: 'products/get failed' })
  }
}

export const deleteProduct = (id: number): AppThunk => async (dispatch) => {
  dispatch({ type: 'product/delete', payload: id})
  try {
    await axios.delete('http://localhost:3001/products/' + id )
    dispatch(deleteProductSuccess(id))
  } catch(e) {
    dispatch({ type: 'products/get failed' })
  }
}
