import { createReducer } from '@reduxjs/toolkit';
import { Product } from '../../../model/product';
import { addProductSuccess, deleteProduct, deleteProductSuccess, getProductsSuccess } from './catalog.actions';

const initialState: Product[] = [

]

export const catalogReducer = createReducer(initialState, builder =>
  builder
    .addCase(getProductsSuccess, (state, action) => {
      return [...action.payload]
    })
    .addCase(addProductSuccess, (state, action) => {
      state.push(action.payload)
    })
    .addCase(deleteProductSuccess, (state, action) => {
      return state.filter(p => p.id !== action.payload)
    })
)
