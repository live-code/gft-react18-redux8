import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../App';
import { Product } from '../../model/product';
import { addToCart } from '../order/store/cart/cart.store';
import { addProduct, deleteProduct, getProducts } from './store/catalog.actions';

export function CatalogPage() {
  const dispatch = useDispatch<AppDispatch>();
  const products = useSelector((state: RootState) => state.catalog);

  useEffect(() => {
    dispatch(getProducts())
  }, [])

  function addProductHandler() {
    const product: Partial<Product> = {
      title: 'Product ' + Math.random(),
      cost: 0
    }
    dispatch(addProduct(product))
  }
  return <div>
    Catalog

    <button onClick={addProductHandler}>ADD</button>
    {
      products.map((p, index) =>
        <li key={index}>
          {p.title}
          <i className="fa fa-cart-plus" onClick={() => dispatch(addToCart(p))}></i>
          <i className="fa fa-trash" onClick={() => dispatch(deleteProduct(p.id))}></i>
        </li>
      )
    }
  </div>
}
