import { createReducer } from '@reduxjs/toolkit';
import * as ShippingActions from './shipping.actions';

export type MaterialType = 'wood' | 'plastic'

interface ShippingInfo {
  itemsPerPallets: number;
  material: MaterialType;
}

const initialState: ShippingInfo = {
  itemsPerPallets: 10,
  material: 'wood',
};

export const shippingReducer = createReducer(initialState, builder => {
  builder
    .addCase(ShippingActions.changePalletType, (state, action) => { state.itemsPerPallets = action.payload })
    .addCase(ShippingActions.changePalletMaterial, (state, action) => {
      state.material = action.payload
    })
})
