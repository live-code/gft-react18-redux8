import { createAction } from '@reduxjs/toolkit';
import { MaterialType } from './shipping.reducer';

export const changePalletType = createAction<number>('counter/changePalletType')
export const changePalletMaterial = createAction<MaterialType>('counter/changePalletMaterial')

