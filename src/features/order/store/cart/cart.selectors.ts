import { RootState } from '../../../../App';

export const getCart = (state: RootState) => state.order.cart;
export const getCartTotal = (state: RootState) => state.order.cart.length;
export const getCartTotalCost = (state: RootState) =>
  state.order.cart.reduce((acc, cartItem) => acc + cartItem.cost, 0)
