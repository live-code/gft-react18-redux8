import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../../../../model/product';

const initialState: Product[] = []

export const cartStore = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addToCart(state, action: PayloadAction<Product>) {
      state.push(action.payload)
    },
    removeFromCart(state, action: PayloadAction<number>) {
      return state.filter(p => p.id !== action.payload)
    }
  }
})

export const { addToCart, removeFromCart } = cartStore.actions;
