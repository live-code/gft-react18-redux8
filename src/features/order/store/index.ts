import { combineReducers } from '@reduxjs/toolkit';
import { cartStore } from './cart/cart.store';
import { shippingReducer } from './shipping/shipping.reducer';

export const orderReducer = combineReducers({
  shipping: shippingReducer,
  cart: cartStore.reducer
});

