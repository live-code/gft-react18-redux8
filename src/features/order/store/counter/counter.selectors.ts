import { RootState } from '../../../../App';

export const getOrder = (state: RootState) => state.order

export const getTotalPallets = (state: RootState) =>
  Math.ceil(state.order.cart.length / state.order.shipping.itemsPerPallets)


export const getPalletsMaterial = (state: RootState) =>
  state.order.shipping.material
