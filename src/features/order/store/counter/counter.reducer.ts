import { createReducer } from '@reduxjs/toolkit';
import * as CounterActions from './counter.actions';

interface Counter {
  value: number;
}

const initialState: Counter = {
  value: 0,

};

export const counterReducer = createReducer(initialState, builder => {
  builder
    .addCase(CounterActions.increment, (state, action) => { state.value += action.payload })
    .addCase(CounterActions.decrement, (state, action) => { state.value -= action.payload })
})
