import { useDispatch, useSelector } from 'react-redux';
import { CartList } from './components/CartList';
import { getCart, getCartTotalCost } from './store/cart/cart.selectors';
import { removeFromCart } from './store/cart/cart.store';
import * as CounterActions  from './store/counter/counter.actions';
import * as ShippingActions from './store/shipping/shipping.actions';
import { getOrder, getPalletsMaterial, getTotalPallets } from './store/counter/counter.selectors';

export function OrderPage() {
  const dispatch = useDispatch();
  const order = useSelector(getOrder);
  const cart = useSelector(getCart)
  const cartTotalCost = useSelector(getCartTotalCost);
  const totalPallets = useSelector(getTotalPallets )
  const material = useSelector(getPalletsMaterial)

  function sendOrder() {
    console.log(order)
  }

  return <div>
    <h1>Total Cart: {cart.length}</h1>
    <CartList
      data={cart}
      onDelete={(id) => dispatch(removeFromCart(id))}
    />
    TOTAL: {cartTotalCost}
    <hr/>

    <h2>Pallets: {totalPallets} of {material} </h2>
    <button onClick={() => dispatch(CounterActions.increment(2))}>+2</button>
    <button onClick={() => dispatch(ShippingActions.changePalletType(5))}>Pallet da 5</button>
    <button onClick={() => dispatch(ShippingActions.changePalletType(10))}>Pallet da 10</button>
    <button onClick={() => dispatch(ShippingActions.changePalletMaterial('wood'))}>wood</button>
    <button onClick={() => dispatch(ShippingActions.changePalletMaterial('plastic'))}>plastic</button>
    <hr/>
    <button onClick={sendOrder}>ORDER</button>

  </div>
}
