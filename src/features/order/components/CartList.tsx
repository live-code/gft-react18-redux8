import { Product } from '../../../model/product';

interface CartListProps {
  data: Product[];
  onDelete: (id: number) => void;
}

export function CartList(props: CartListProps) {
  return <div>
    {
      props.data.map((p, index) => {
        return <li key={index}>
          {p.title} - € {p.cost}
          <i className="fa fa-trash"
             onClick={() => props.onDelete(p.id)}></i>
        </li>
      })
    }
  </div>
}
