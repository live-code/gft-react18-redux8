import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { changeLanguage, changeTheme } from '../../core/store/app.store';

export function SettingsPage() {
  const dispatch = useDispatch()

  return <div>
    Settings
    <button onClick={ () => dispatch(changeLanguage('it')) }>IT</button>
    <button onClick={ () => dispatch(changeLanguage('en')) }>EN</button>
    <hr/>
    <button onClick={ () => dispatch(changeTheme('dark')) }>DARK</button>
    <button onClick={ () => dispatch(changeTheme('light')) }>LIGHT</button>
  </div>
}
