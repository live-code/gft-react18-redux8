export type Language = 'it' | 'en' | 'de' | 'es';
export type Theme = 'dark' | 'light';

export interface AppState {
  theme: Theme;
  language: Language;
}
