export interface Product {
  id: number;
  title: string;
  cost: number
}
