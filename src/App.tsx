import { Action, AnyAction, combineReducers, configureStore, ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Link, Navigate, Route, Routes } from 'react-router-dom';
import { Navbar } from './core/components/NavBar';

import { appStore } from './core/store/app.store';
import { CatalogPage } from './features/catalog/CatalogPage';
import { catalogReducer } from './features/catalog/store/catalog.reducer';
import { OrderPage } from './features/order/OrderPage';
import { orderReducer } from './features/order/store';
import { HomePage } from './features/home/HomePage';
import { SettingsPage } from './features/settings/SettingsPage';
import { userStore } from './features/users/store/users.store';
import { UsersPage } from './features/users/UsersPage';

const rootReducer = combineReducers({
  appConfig: appStore.reducer,
  catalog: catalogReducer,
  order: orderReducer,
  // users: usersReducer
  users: userStore.reducer
})

export const store = configureStore({
  reducer: rootReducer
})

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = ThunkDispatch<RootState, null, AnyAction>
export type AppThunk = ThunkAction<void, RootState, null, AnyAction>;

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/home" element={ <HomePage />} />
          <Route path="/order" element={ <OrderPage />} />
          <Route path="/settings" element={ <SettingsPage />} />
          <Route path="/users" element={ <UsersPage />} />
          <Route path="/catalog" element={ <CatalogPage />} />
          <Route path="*"  element={
            <Navigate to="/home" />
          }/>
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
