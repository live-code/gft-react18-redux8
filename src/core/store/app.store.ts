import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppState, Language, Theme } from '../../model/app-state';

const initialState: AppState = { theme: 'dark', language: 'it'}

export const appStore = createSlice({
  name: 'app',
  initialState,
  reducers: {
    changeTheme(state, action: PayloadAction<Theme>) {
      state.theme = action.payload
    },
    changeLanguage(state, action: PayloadAction<Language> ) {
      state.language = action.payload;
    }
  }
})

export const {
  changeLanguage,
  changeTheme
} = appStore.actions;
