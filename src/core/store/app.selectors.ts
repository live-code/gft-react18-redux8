import { RootState } from '../../App';

export const getTheme = (state: RootState) => state.appConfig.theme;
export const getLanguage = (state: RootState) => state.appConfig.language;
