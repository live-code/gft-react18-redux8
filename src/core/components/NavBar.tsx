import classNames from 'classnames';
import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RootState } from '../../App';
import { getCart, getCartTotal, getCartTotalCost } from '../../features/order/store/cart/cart.selectors';
import { getTotalPallets } from '../../features/order/store/counter/counter.selectors';
import { getLanguage, getTheme } from '../store/app.selectors';

const activeStyle: React.CSSProperties = { color: 'orange'};

export const Navbar = () => {
  const theme = useSelector(getTheme);
  const lang = useSelector(getLanguage);
  const totalCart = useSelector(getCartTotal)
  const cartTotalCost = useSelector(getCartTotalCost);

  return (
    <nav
      className={classNames(
        'navbar navbar-expand',
        {
          'navbar-light bg-light': theme === 'light',
          'navbar-dark bg-dark': theme === 'dark',
        }
      )}
    >
      <div className="navbar-brand">
        <NavLink
          style={({ isActive }) => isActive ? activeStyle : {}}
          className="nav-link"
          to="/">
          REDUX {lang === 'it' ? '🇮🇹' : '🇬🇧'}
        </NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink style={(obj) => obj.isActive ? activeStyle  : {} }
              className="nav-link"
              to="/settings">
              <small>settings</small>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/order">
              <small>
                order
                ({totalCart} - € {cartTotalCost})
              </small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/users">
              <small>users</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/catalog">
              <small>catalog</small>
            </NavLink>
          </li>

        </ul>
      </div>
    </nav>
  )
}
